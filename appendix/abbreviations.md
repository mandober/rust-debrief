# Abbreviations


## TA
Type annotation

## GTP
Generic type parameter

## GLP
Generic lifetimes parameter

## GLA
Generic lifetimes annotation

## LER
Lifetime elision rules (LER1, LER2, LER3)

## DST
Dynamically sized type

## ZST
Zero-sized type

## BET
Behavior-enforcing trait

## ref
A reference

## deref
to derefrence

## ADT
Algebraic data type

## SDT
Structural data type

## NDT
Nominal data type