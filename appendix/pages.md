# PAGES

## Theory
- Type Systems
  - Data Types
  - Algebraic Data Types
- Binary relations
  - Partial equality
  - Partial ordering
- Hashing
- Unicode
- Memory management
  - Memory hierarchy
  - Types of memory (SRAM, DRAM)
  - Address space
  - Calling convention
  - The Stack
  - The Heap
  - Alignment
  - Word
  - Pointer
  - Program memory
  - CPU
  - Registers
  - Cache
- Programming Paradigms
  - Inheritance
  - Interface
  - Polymorphism
  - Generics
  - Reflection

## Core
- Low level
  - ISA, x86
  - Compilers, IR, Object code
  - The Rust Language
  - The Compiler
  - HIR, MIR, LLVM IR
  - Internals
  - Feature Gates
- Libraries
  - core
  - alloc
  - std_unicode
  - proc_macro
- The Tools
  - cargo
  - rustup
  - rustdoc
- Third Party Crates


## Syntax
- Attributes
- Comments
- Constraints
- Conventions
- Expressions
- Control flow
- Formatting output
- Fully Qualified Syntax
- Grammar
- Keywords
- Literals
- Macros
- Operators
- Syntax

## Semantics
- Ownership
- Binding
- Move
- Copy
- Borrowing
- Mutability
- Functions
- Pattern matching
- Lifetimes
- Iterators
- Visibility

## Primitives
- Scalars
  - booleans
  - integers
  - machine dependent integers
  - floats
  - characters
- Array
- Tuple
- Pointer types
  - references
  - raw pointers
  - function pointers
  - Slices
    - slice
    - string slice

## Types
- Categories
- Unit
- Never
- Composite types
- Dynamically Sized Types
- Zero Sized Types
- Empty Types
- Top

## Modules
- methods on primitives
- types and methods
  - Option
  - Box
  - Option
  - Result
  - Vec
  - Box
- collections
- traits
- env and IO
- other modules

## Traits
- Index
- Index by module
- Derivable traits
- Display

## Items
- Lang Items
- extern crate declarations
- extern blocks
- use declarations
- modules
- constant items
- static items
- function definitions
- type definitions
- struct definitions
- enum definitions
- union definitions
- trait definitions
- implementations


## Appendix
- Glossary
- Terminology
- Abbreviations
- Pages
- Index


## Links
- Official
- Community
- Resources
