# Rust Debrief

Rust by bullet point: from summary to particularity

- [GitHub](https://github.com/mandober/rust-debrief)
- [GitBooks](https://mandober.gitbooks.io/rust-debrief)

Rust, debriefings and details: notes, lists, reminders, quick reference, cheat-sheets and other by-products of oxidation.

```rust
impl<T> Debriefing for Topic<T> where T: Rust {
  fn debriefing(&self) -> Summary;
}

impl<T> Details for Topic<T> where T: Rust {
  fn details(&self) -> Particularity;
}
```

Material contained is aggregated primarily from Rust's official documentation, both versions of the Rust Book, Rust Reference, Rustonomicon, Unstable Book, Rust by Example, Rust Cookbook, various RFCs, Rust FAQ, Rust internals forum, Rust user forum, Rust grammar, Rust style guide, rustdoc book, Cargo book, Rust official blog, Compiler Error Index, as well as from many other places I encounter while studying Rust. A work in progress never to be completed.


Project:
- title: Rust Debrief
- subtitle: Rust by bullet point: from summary to particularity
- desc: Rust, debriefings and details: notes, lists, reminders, quick reference, cheat-sheets and other by-products of oxidation.
- language: Rust
- repo: https://github.com/mandober/rust-debrief
- GitBooks: https://mandober.gitbooks.io/rust-debrief
- author: Ilic Ivan
- about: Rust learning, notes taking: documenting the process of studying Rust
- date: `'my`
- note: still unstructured
