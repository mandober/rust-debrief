# Quotes

_"A programming language is low level when its concepts require attention to the irrelevant"._   
-- Alan Perlis

_"You think you know when you learn, are more sure when you can write, even more when you can teach, but certain when you can program"._   
-- Alan Perlis

_"Perhaps the central problem we face in all of computer science is how we are to get to the situation where we build on top of the work of others rather than redoing so much of it in a trivially different way. Science is supposed to be cumulative, not almost endless duplication of the same kind of things."_   
-- Richard Hamming

_"The sooner you start to code, the longer the program will take"._   
-- Roy Carlson

_"Whenever possible, steal code"._   
-- Tom Duff 

_"If you find that you're spending almost all your time on theory, start turning some attention to practical things; it will improve your theories. If you find that you're spending almost all your time on practice, start turning some attention to theoretical things; it will improve your practice"._   
-- Donald Knuth

