# The Rust Teams

The Rust project is governed by a number of teams, each focused on a specific area of concern:
- Core team is responsibile for overall direction of the project, subteam leadership, cross-cutting concerns
- Language team: designing new language features
- Language team shepherds: helping guide language RFCs to completion, assisting the language team with its work
- Library team: the Rust standard library, rust-lang crates, conventions, and ecosystem support
- Compiler team: compiler internals, optimizations
- Dev tools team: overall direction for tools for working with Rust code
- Dev tools peers: oversight of specific Rust tools, and coordination with dev tools team
Cargo team
Responsibility: design and implementation of Cargo

Infrastructure team
Responsibility: infrastructure supporting the Rust project itself: CI, releases, bots, metrics

Release team
Responsibility: tracking regressions, stabilizations, and producing Rust releases

Community team
Responsibility: coordinating events, outreach, commercial users, teaching materials, and exposure

Documentation team
Responsibility: ensuring Rust has fantastic documentation

Documentation peers
Responsibility: oversight of specific documentation, and coordination with the docs team

Moderation team
Responsibility: helping uphold the code of conduct

Style team
Responsibility: temporary 'strike team' charged with deciding on code style guidelines and configuring Rustfmt (process specified in RFC 1607) 

