# Associated items

Associated items:
- Associated types
- Associated functions
- Associated lifetimes
- Associated constants


Rust's [RFC 195](https://github.com/rust-lang/rfcs/blob/master/text/0195-associated-items.md) defines support for associated items (functions, statics, types, lifetimes) in trait declarations. All associated items can have default values.




- https://github.com/rust-lang/rfcs/blob/master/text/0195-associated-items.md
- https://github.com/nox/rust-rfcs/blob/master/text/0195-associated-items.md
- https://github.com/rust-lang/rfcs/blob/master/text/1598-generic_associated_types.md
- https://internals.rust-lang.org/t/rfc-0195-associated-items-and-const-fn/4518
- https://github.com/rust-lang/rfcs/blob/master/text/0195-associated-items.md
- http://smallcultfollowing.com/babysteps/blog/2016/11/02/associated-type-constructors-part-1-basic-concepts-and-introduction/
- https://www.reddit.com/r/rust/comments/2deaqh/rfc_associated_items_and_multidispatch_traits/
- https://news.ycombinator.com/item?id=14915539
