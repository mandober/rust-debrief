# Language Constructs

A language construct is a syntactically allowable part of a program that may be formed from one or more lexical tokens in accordance with the rules of a programming language. It is the way a programming language is written.

