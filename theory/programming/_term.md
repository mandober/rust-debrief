# Programming Paradigms

<!-- TOC -->

- [Marker interfaces](#marker-interfaces)
- [Reflection](#reflection)
- [Invariant](#invariant)
- [Standard Library](#standard-library)
- [Undefined behavior](#undefined-behavior)

<!-- /TOC -->

## Marker interfaces
Marker interfaces contain no methods at all and serve to provide run-time information to generic processing using reflection. 

## Reflection
is the ability of a computer program to examine, introspect, and modify its own structure and behavior at runtime.

## Invariant
an invariant is a condition that can be relied upon to be true during execution of a program, or during some portion of it. It is a logical assertion that is held to always be true during a certain phase of execution. For example, a loop invariant is a condition that is true at the beginning and end of every execution of a loop.

## Standard Library
A standard library is the library made available across implementations of a programming language. It is often treated as part of the language by its users, although the designers may have treated it as a separate entity. The line between a language and its libraries differs between languages and it is not always clear. Some languages are designed so that the meanings of certain syntactic constructs cannot even be described without referring to the core library.

## Undefined behavior
Undefined behavior (UB) is the result of executing computer code whose behavior is not prescribed by the language specification.
