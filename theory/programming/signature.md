# Signature

A _type signature_ or type annotation defines the inputs and outputs for a function, subroutine or method. A type signature includes the number of arguments, the types of arguments and the order of the arguments contained by a function. A type signature is typically used during overload resolution for choosing the correct definition of a function to be called among many overloaded forms.

A _function signature_ consists of the function prototype; it specifies the general information about a function like the name, parameters, return type, privacy, etc.

A _file signatures_ identifies or verifies the content of a file. A _database signatures_ can identify or verify the schema or a version of a database. In the ML family of programming languages, keyword `signature` refers to a construct of the module system that plays the role of an interface. A method is commonly identified by its unique _method signature_, which usually includes its name and the types, number and order of its parameters.
