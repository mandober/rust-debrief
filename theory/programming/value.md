# Value

A value is the representation of some entity that can be manipulated by a program. The members of a type are the values of that type.

The "value of a variable" is given by the corresponding mapping in the environment. In languages with assignable variables it becomes necessary to distinguish between the r-value (or contents) and the l-value (or location) of a variable.

In imperative programming languages, values can generally be accessed or changed at any time. In pure functional and logic languages, variables are bound to expressions and keep a single value during their entire lifetime due to the requirements of referential transparency. In imperative languages, the same behavior is exhibited by (named) constants (symbolic constants), which are typically contrasted with (normal) variables. 

In declarative (high-level) languages, values have to be referentially transparent. This means that the resulting value is independent of the location in which a (sub)expression needed to compute the value is stored. Only the contents of the location (the bits, whether they are 1 or 0) and their interpretation are significant.
