# Programming

- [machine-code](machine-code.md)






https://www.wikiwand.com/en/Data_structure
https://www.wikiwand.com/en/Tree_(data_structure)



Languages with manual memory management: Ada, C, C++, Fortran, Pascal and Rust.

## Language constructs and concepts
Assignment
Basic syntax
Basic instructions
Comments
Control flow
Foreach loops
While loops
For loops
Do-while
Exception handling
Enumerated types
Anonymous functions
Conditional expressions
Functional instructions
Arrays
Associative arrays
String operations
String functions
Higher-order functions
Type systems
List comprehension
Object-oriented programming
Object-oriented constructors
Operators