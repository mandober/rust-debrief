# Programming languages


A programming language is a _formal language_ that specifies a set of instructions that can be used to produce various kinds of output. 

Programming languages generally consist of instructions for a computer. Programming languages can be used to create programs that implement specific algorithms.

The description of a programming language is usually split into the two components of _syntax_ (form) and _semantics_ (meaning).

A programming language is a notation for writing programs, which are specifications of a computation or algorithm.

Programming languages usually contain abstractions for defining and manipulating data structures or controlling the flow of execution. The practical necessity for adequate abstractions is expressed by the _abstraction principle_ a.k.a Don't Repeat Yourself (DRY).

All programming languages have some _primitive_ building blocks for the description of data and the processes or transformations applied to them. These primitives are defined by syntactic and semantic rules which describe their structure and meaning respectively. 

Elements of a programming languages are divided in syntax and semantic.

---

All programming languages have some primitive building blocks for the description of data and the processes or transformations applied to them. These primitives are defined by syntactic and semantic rules which describe their structure and meaning respectively.

The **syntax** is the set of rules that defines the allowed combinations of symbols. **Semantics** refers to the meaning of languages, as opposed to their form.

An **expression** is a combination of one or more literals, constants, variables, operators, and functions that the programming language interprets and computes to produce another value. This process is called evaluation.

A **statement** is the smallest standalone element that expresses some action to be carried out. A statement may have internal components (e.g. expressions). Many languages make a distinction between statements and definitions, with a statement only containing executable code and a **definition** instantiating an identifier, while an expression evaluates to a value only. A distinction can also be made between simple and compound statements; the latter may contain statements as components.

> An expression is evaluated, a statement is executed, definition (declaration) instantiates an identifier.



Elements: 
- syntax
- semantics
  - Static semantics
  - Dynamic semantics
- Type system
  - Typed versus untyped languages
  - Static versus dynamic typing
  - Weak and strong typing
- Standard library and run-time system


