# Computer science

https://www.wikiwand.com/en/Computer_science
http://interactivepython.org/runestone/static/pythonds/Introduction/WhatIsComputerScience.html


Computer science is the study of the theory, experimentation and engineering that form the basis for the design and use of computers.

It is the scientific and practical approach to computation and its applications and the systematic study of the feasibility, structure, expression and mechanization of the methodical procedures (or algorithms) that underlie the acquisition, representation, processing, storage, communication of, and access to, information.

An alternate, more succinct definition of computer science is the study of automating algorithmic processes that scale.

A computer scientist specializes in the theory of computation and the design of computational systems. Its fields can be divided into a variety of theoretical and practical disciplines.

Some fields, such as computational complexity theory (which explores the fundamental properties of computational and intractable problems), are highly abstract, while fields such as computer graphics emphasize real-world visual applications.

Other fields still focus on challenges in implementing computation.

For example, programming language theory considers various approaches to the description of computation, while the study of computer programming itself investigates various aspects of the use of programming language and complex systems.

Human–computer interaction considers the challenges in making computers and computations useful, usable, and universally accessible to humans.


## Areas and fields
As a discipline, computer science spans a range of topics from theoretical studies of algorithms and the limits of computation to the practical issues of implementing computing systems in hardware and software.

Four areas considered crucial to the discipline of computer science:
1. Theory of computation
2. Algorithms and data structures
3. Programming methodology and languages
4. Computer elements and architecture.

In addition, these fields are important areas of computer science:
- engineering
- artificial intelligence
- computer networking and communication
- database systems
- parallel computation
- distributed computation
- human–computer interaction
- computer graphics
- operating systems
- numerical and symbolic computation

---

Computer science is the study of problems, problem-solving, and the solutions that come out of the problem-solving process. Given a problem, a computer scientist’s goal is to develop an algorithm, a step-by-step list of instructions for solving any instance of the problem that might arise. Algorithms are finite processes that if followed will solve the problem. Algorithms are solutions.

Programming is the process of taking an algorithm and encoding it into a notation, a programming language, so that it can be executed by a computer. Although many programming languages and many different types of computers exist, the important first step is the need to have the solution. Without an algorithm there can be no program.

All data items in the computer are represented as strings of binary digits. In order to give these strings meaning, we need to have data types. Data types provide an interpretation for this binary data so that we can think about the data in terms that make sense with respect to the problem being solved. These low-level, built-in data types (sometimes called the primitive data types) provide the building blocks for algorithm development.

An _abstract data type_, sometimes abbreviated ADT, is a logical description of how we view the data and the operations that are allowed without regard to how they will be implemented. This means that we are concerned only with what the data is representing and not with how it will eventually be constructed. By providing this level of abstraction, we are creating an encapsulation around the data. The idea is that by encapsulating the details of the implementation, we are hiding them from the user’s view. This is called information hiding.

The implementation of an abstract data type, often referred to as a _data structure_, will require that we provide a physical view of the data using some collection of programming constructs and primitive data types.

The separation of these two perspectives will allow us to define the complex data models for our problems without giving any indication as to the details of how the model will actually be built.

This provides an implementation-independent view of the data. Since there will usually be many different ways to implement an abstract data type, this implementation independence allows the programmer to switch the details of the implementation without changing the way the user of the data interacts with it. The user can remain focused on the problem-solving process.


