# Machine code

Machine code is the only language a computer can process directly without a previous transformation. True machine code is a stream of raw binary data. A programmer coding in "machine code" normally codes instructions and data in a more readable form such as hexadecimal which is translated to internal format by a program called a loader.

