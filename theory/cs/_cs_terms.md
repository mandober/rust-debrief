
## Object Code
In a general sense **object code** is a compiler produced sequence of statements or instructions in a computer language, usually a machine code language or an intermediate language such as register transfer language (RTL).


## Optimizing Compiler
An **optimizing compiler** is a compiler that tries to minimize or maximize some attributes of an executable computer program. The most common requirement is to minimize the time taken to execute a program; a less common one is to minimize the amount of memory occupied.
