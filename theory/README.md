# Bit chasing


- [Type Systems](type-systems.md)
  - [Data Types](data-types.md)
  - [Algebraic Data Types](algebraic.md)
- [Binary relations](binary-relations.md)
  - [Equivalence relation](binary-relations.md#equivalence-relation)
  - [Partial equivalence relation](binary-relations.md#partial-equivalence-relation)
  - Partial ordering
- [Unicode](unicode.md)
- Memory management
  - Pointers
  - Address space
  - Alignment
  - The Stack
  - The Heap
- Programming Paradigms
  - Inheritance
  - Interface
  - Polymorphism
  - Generics
  - Reflection
- Low Level
  - ISA, x86
  - Compilers, IR, Object code





> "_If you find that you're spending almost all your time on practice, start turning some attention to theoretical things; it will improve your practice_." -- Donald Knuth
