# Memory

- [Memory hierarchy](01_memory-hierarchy.md)
- [Types of memory](02_memory-types.md)

- Alignment
- Address space
- Registers
- Calling convention
- Word
- Program memory
- Stack
- Heap
