# bit

The prevalent unit of information is the **bit**, a portmanteau of **BI**nary digi**T**, though not the same: a binary digit is a number that can take one, out of two possible values (0 or 1), whereas a bit is the maximum amount of information that can be conveyed by a binary digit. By analogy, a binary digit is like a container, whereas information is the amount of matter in the container. One bit is typically defined as the information entropy of a random binary variable that is 0 or 1 with equal probability, or the information that is gained when the value of such variable becomes known.


## Derived Units
Today, a **byte** almost always means 8 bits, an octet. A byte can represent 256 distinct states (a bit has 2 variants, 8 bits have 2^8 variants). Half of byte, 4 bits, is called a **nibble** and one hexadecimal digit can represent all of its 16 variants (2^4).


## Word, block, and page
Computers usually manipulate bits in groups of a fixed size, conventionally called **words**. The number of bits in a word is defined by the size of the registers in CPU or by the number of data bits that can be fetched from the main memory in a single operation. Some machine instructions and computer number formats use two words (double word, dword), or four words (quad word, quad). Computer memory caches usually operate on blocks of memory that consist of several consecutive words. These units are customarily called **cache blocks**, or, in CPU caches, **cache lines**. Virtual memory systems partition the computer's main storage into even larger units, traditionally called **pages**.

https://www.wikiwand.com/en/Word_(computer_architecture)

