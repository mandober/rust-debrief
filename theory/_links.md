# Links


* Language_construct: https://www.wikiwand.com/en/Language_construct

- Value:  https://www.wikiwand.com/en/Value_(computer_science)
- Variable: https://www.wikiwand.com/en/Variable_(computer_science)
  - Identifier: https://www.wikiwand.com/en/Identifier#/In_computer_languages
  - Non-local_variable: https://www.wikiwand.com/en/Non-local_variable
  - Free_variable: https://www.wikiwand.com/en/Free_variable
  - Global_variable: https://www.wikiwand.com/en/Global_variable
- Declaration: https://www.wikiwand.com/en/Declaration_(computer_programming)
  - Forward_declaration: https://www.wikiwand.com/en/Forward_declaration
- Assignment: https://www.wikiwand.com/en/Assignment_(computer_science)
  - Destructive_assignment: https://www.wikiwand.com/en/Destructive_assignment
- Binding: https://www.wikiwand.com/en/Name_binding
  - Dynamic_binding: https://www.wikiwand.com/en/Dynamic_binding_(computing)
  - Static_binding: https://www.wikiwand.com/en/Static_binding
  - Late_binding: https://www.wikiwand.com/en/Late_binding
  - Virtual_method_table: https://www.wikiwand.com/en/Virtual_method_table



- Primitive_data_type: https://www.wikiwand.com/en/Primitive_data_type
- Complex_data_type: https://www.wikiwand.com/en/Complex_data_type
- Reference: https://www.wikiwand.com/en/Reference_(computer_science)
- Pointer: https://www.wikiwand.com/en/Pointer_(computer_programming)
  - Null_pointer: https://www.wikiwand.com/en/Null_pointer


- Data_type: https://www.wikiwand.com/en/Data_type
- Type system: https://www.wikiwand.com/en/Type_system
- Type_signature: https://www.wikiwand.com/en/Type_signature



- Covariance_and_contravariance: https://www.wikiwand.com/en/Covariance_and_contravariance_(computer_science)

- Constant: https://www.wikiwand.com/en/Constant_(computer_programming)
- Operator: https://www.wikiwand.com/en/Operator_(programming)
- Expression: https://www.wikiwand.com/en/Expression_(programming)
- Statement: https://www.wikiwand.com/en/Statement_(computer_science)
- Subroutine: https://www.wikiwand.com/en/Subroutine
- Closure: https://www.wikiwand.com/en/Closure_(computer_science)

- Name_mangling: https://www.wikiwand.com/en/Name_mangling

- Evaluation: https://www.wikiwand.com/en/Evaluation_(computer_science)
- Side_effect_: https://www.wikiwand.com/en/Side_effect_(computer_science)
- Exception_handling: https://www.wikiwand.com/en/Exception_handling
- State: https://www.wikiwand.com/en/State_(computer_science)
- Immutable_object: https://www.wikiwand.com/en/Immutable_object

- Memory_hierarchy: https://www.wikipedia.com/en/Memory_hierarchy
- Memory address: https://www.wikiwand.com/en/Memory_address
- Word: https://www.wikiwand.com/en/Word_(computer_architecture)
- X86_calling_conventions: https://www.wikiwand.com/en/X86_calling_conventions


- Interpreter: https://www.wikiwand.com/en/Interpreter_(computing)
- Compiler: https://www.wikiwand.com/en/Compiler
- Optimizing_compiler: https://www.wikiwand.com/en/Optimizing_compiler
- Correctness: https://www.wikiwand.com/en/Correctness_(computer_science)
- Memoization: https://www.wikiwand.com/en/Memoization
- Common_subexpression_elimination: https://www.wikiwand.com/en/Common_subexpression_elimination
- Static_code_analysis: https://www.wikiwand.com/en/Static_code_analysis
- Definite_clause_grammar: https://www.wikiwand.com/en/Definite_clause_grammar
- Lazy_evaluation: https://www.wikiwand.com/en/Lazy_evaluation
- Parallelization: https://www.wikiwand.com/en/Parallelization
- Code_motion: https://www.wikiwand.com/en/Code_motion
- Referential_transparency: https://www.wikiwand.com/en/Referential_transparency
- Execution: https://www.wikiwand.com/en/Execution_(computing)
- Compile-time: https://www.wikiwand.com/en/Compile_time
- Link-time: https://www.wikiwand.com/en/Link_time
- Load-time: https://www.wikiwand.com/en/Load_time
- Run-time: https://www.wikiwand.com/en/Run_time_(program_lifecycle_phase)


- Computer program: https://www.wikiwand.com/en/Computer_program
- Programming_language: https://www.wikiwand.com/en/Programming_language
- Programming_language_theory: https://www.wikiwand.com/en/Programming_language_theory
- Domain_theory: https://www.wikiwand.com/en/Domain_theory


- Generic_programming: https://www.wikiwand.com/en/Generic_programming
- Parametric_polymorphism: https://www.wikiwand.com/en/Parametric_polymorphism
- Ad_hoc_polymorphism: https://www.wikiwand.com/en/Ad_hoc_polymorphism


- Optimization: https://www.wikiwand.com/en/Optimization_(computer_science)
- Abstraction_principle: https://www.wikiwand.com/en/Abstraction_principle_(computer_programming)
- Syntax: https://www.wikiwand.com/en/Syntax_(programming_languages)
- Semantics: https://www.wikiwand.com/en/Semantics_(computer_science)
- Context-free_grammar: https://www.wikiwand.com/en/Context-free_grammar
- Backus-Naur form: https://www.wikiwand.com/en/Backus%E2%80%93Naur_form
- Abstraction: https://www.wikiwand.com/en/Abstraction_(software_engineering)
- Pseudocode: https://www.wikiwand.com/en/Pseudocode


---
## Paradigms
- Imperative_programming: https://www.wikiwand.com/en/Imperative_programming
- Declarative_programming: https://www.wikiwand.com/en/Declarative_programming
- Structured_programming: https://www.wikiwand.com/en/Structured_programming
- General-purpose_programming_language: https://www.wikiwand.com/en/General-purpose_programming_language
- Modeling_language: https://www.wikiwand.com/en/Modeling_language


---

- Machine code: https://www.wikiwand.com/en/Machine_code
- Universal Turing machine: https://www.wikiwand.com/en/Universal_Turing_machine
- Finite-state_machine: https://www.wikiwand.com/en/Finite-state_machine
- Algorithm: https://www.wikiwand.com/en/Algorithm
- Von_Neumann_architecture: https://www.wikiwand.com/en/Von_Neumann_architecture




https://www.wikiwand.com/en/Order_theory
https://www.wikiwand.com/en/Partially_ordered_set
https://www.wikiwand.com/en/Equivalence_class
https://www.wikiwand.com/en/Equivalence_relation
https://www.wikiwand.com/en/Partial_equivalence_relation





---



## primitive
A *data primitive* (or just primitive) is any datum that can be read from or written to computer memory using one memory access (for instance, both a byte and a word are primitives).

## aggregate
A *data aggregate* (or just aggregate) is a group of primitives that are logically contiguous in memory and that are viewed collectively as one datum (for instance, an aggregate could be 3 logically contiguous bytes, the values of which represent the 3 coordinates of a point in space). When an aggregate is entirely composed of the same type of primitive, the aggregate may be called an array; in a sense, a multi-byte word primitive is an array of bytes, and some programs use words in this way.

In the context of these definitions, a byte is the smallest primitive; each memory address specifies a different byte. The memory address of the initial byte of a datum is considered the memory address (or base memory address) of the entire datum.
