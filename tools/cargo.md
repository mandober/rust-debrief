# Cargo



Cargo is a tool that allows Rust projects to declare their various dependencies and ensure that you’ll always get a repeatable build. It is part of the Rust toolchain that is installed with rustup. This project was bootstrapped with the following command:

`cargo new project`