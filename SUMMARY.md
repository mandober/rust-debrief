# Summary

- [Rust Debrief](README.md)


## Theory
- [Type Systems](theory/type-theory/type-systems.md)
- [Data Types](theory/type-theory/data-types.md)


## Syntax
- [Attributes](syntax/attributes.md)
- [Comments](syntax/comments.md)
- [Conventions](syntax/conventions.md)
- [Expressions](syntax/expressions.md)
- [Control flow](syntax/control-flow.md)
- [Formatting output](syntax/format.md)
- [Fully Qualified Syntax](syntax/fully-qualified-syntax.md)
- [Grammar](syntax/grammar.md)
- [Keywords](syntax/keywords.md)
- [Literals](syntax/literals.md)
- [Operators](syntax/operators.md)
- [Syntactic elements](syntax/syntactic-elements.md)


## Semantics
- [Lifetimes](semantics/lifetimes/1_lifetimes.md)
- [Visibility](semantics/visibility.md)
- [Iterators](semantics/iterators.md)

## Primitives
- [Character](primitives/char/char.md)
- [Slices](primitives/slice/slice.md)
- [Unit](primitives/unit/unit.md)
- [Never](primitives/never/never.md)

## Types
- [Classification](types/type-groups.md)
- [Annotation](types/type-annotation.md)
- [Top](types/type_top.md)

## Modules
- [collections](modules/collections/README.md)
- [option](modules/option/README.md)
- [any](modules/any/any.md)
  - [any](modules/any/any-trait.md)
- [iter](modules/iter/README.md)
  - [methods](modules/iter/methods-all.md)
- [box](modules/boxed/box.md)


## Traits
- [Traits](traits/README.md)
- [Index by module](traits/traits-by-mod.md)
- [Derivable traits](traits/derivable-traits.md)
- [Trait resolution](core/trait-resolution.md)


## Items
- [Lang Items](items/README.md)


## Macros
- [Macros](macros/macro.md)
- [Declerative macros](macros/macro-declerative.md)
- [Procedural macros](macros/macro-procedural.md)
- [Index of std macros](macros/macro-index.md)


## Rust Core
- [The compiler](core/compiler.md)
- [Trait resolution](core/trait-resolution.md)
- [Type inference engine](core/type-inference-engine.md)
- [Types and the Type Context](core/types-and-the-type-context.md)
- [MIR](core/mir.md)
- [HIR](core/hir.md)
- [The Borrow Checker](core/borrow-checker.md)
- [Variance of type and lifetime parameters](core/variance-of-type-and-lifetime-parameters.md)


## Appendix
- [Terminology](appendix/terminology.md)
- [Abbreviations](appendix/abbreviations.md)
- [Pages](appendix/pages.md)
- [Index](appendix/index.md)

## Links
- [Links](links/README.md)

