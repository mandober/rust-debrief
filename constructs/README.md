# Language Constructs


1. Entities
  - variable
  - constant
  - static
  - fn
  - trait
  - module

2. Declaration
  - declaration
  - definition
  - forward declaration
  - explicit
  - implicit
  - entity
  - identifier
    - identifier kind
    - identifier properties
      - descriptive
      - prescriptive

3. Binding
  - initialization
  - re-initialization
  - deinitialization
  - binding
    - early binding
    - late binding



---

value
variable
allocation
memory address/location
stack/heap