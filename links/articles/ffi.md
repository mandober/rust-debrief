# FFI


- [Rust FFI C string handling][02]
- [FFI in Rust: writing bindings for `libcpuid`][05]
- 24 days of Rust: [Calling Rust from other languages][06]
- [C types in Rust][01]
- [How to get a `*mut c_char` from a string][03]
- [Rust Once, Run Everywhere][04]



[01]: http://doc.rust-lang.org/1.0.0-beta.2/libc/types/os/arch/c95/
[02]: http://stackoverflow.com/questions/24145823/rust-ffi-c-string-handling
[03]: http://stackoverflow.com/questions/28094636/how-do-i-get-a-mut-c-char-from-a-str
[04]: http://blog.rust-lang.org/2015/04/24/Rust-Once-Run-Everywhere.html
[05]: http://siciarz.net/ffi-rust-writing-bindings-libcpuid/
[06]: http://siciarz.net/24-days-of-rust-calling-rust-from-other-languages/
