# Semantics

- [Functions](functions/README.md)

* Binding
* Move Semantics
* Copy Semantics
* Borrowing
* Mutability
* Pattern matching
* Lifetimes
